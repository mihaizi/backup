function removeOrderItem(orderInfo, position){
    if (!Array.isArray(orderInfo.items)){
        throw new Error('Items should be an array');
    }
	
	for (const prop in orderInfo.items) {
		if (typeof(orderInfo.items[prop].price) === 'undefined' ||
			typeof(orderInfo.items[prop].quantity) === 'undefined' ){
			throw new Error('Malformed item');
		}
	}
	
	if (!(position in orderInfo.items))
		throw new Error('Invalid position');
	
	delete orderInfo.items[position];
	
	orderInfo.total = 0;
	for (const prop in orderInfo.items) {
		orderInfo.total += orderInfo.items[prop].price * orderInfo.items[prop].quantity;
	}
	
	return orderInfo;
	
}

const app = {
    removeOrderItem
};

module.exports = app;